// Fill out your copyright notice in the Description page of Project Settings.


#include "AIctrlBase.h"
#include "playerPawnBase.h"

void AAIctrlBase::BeginPlay()
{
	Super::BeginPlay();

	pawnBase = Cast<AplayerPawnBase>(GetPawn());
	pawnBase->HealthChangedEvent.AddDynamic(this, &AAIctrlBase::pawnHit);
}

void AAIctrlBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	switch (state) {
	case AIStates::acercarse:
		pawnBase->MoveRight(pawnBase->isLookingRight ? 1 : -1);
		break;
	case AIStates::huir:
		pawnBase->MoveRight(pawnBase->isLookingRight ? -1 : 1);
		break;
	case AIStates::hit:
		break;
	case AIStates::idle:
		pawnBase->MoveRight(pawnBase->isLookingRight ? -0.2 : 0.2);
		break;
	}
}

void AAIctrlBase::pawnHit(float newHp)
{
	state = AIStates::hit;
}

void AAIctrlBase::changeState(AIStates newState)
{
	if (state == AIStates::agachado && newState != state) {
		pawnBase->StopCrouch();
	}

	if (newState == AIStates::agachado) {
		pawnBase->DoCrouch();
	}

	state = newState;
}
