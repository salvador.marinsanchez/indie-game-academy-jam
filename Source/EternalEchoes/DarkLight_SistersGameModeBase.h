// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DarkLight_SistersGameModeBase.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGameFinished, int, resul);

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGMReady);


UCLASS()
class ETERNALECHOES_API ADarkLight_SistersGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	

public:
	UPROPERTY(BlueprintReadOnly)
	TArray<class AplayerPawnBase*> players;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isReady = false;

	UPROPERTY(BlueprintAssignable)
	FGameFinished gameFinishedEvent;

	UPROPERTY()
	FGMReady gameModeReady;

	bool isAactorLeft = true;

	ADarkLight_SistersGameModeBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void GameFinished(bool player1wins);
	UFUNCTION(BlueprintCallable)
	void TimerRunOut();


};
