// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCtrlBase.h"
#include "Kismet/GameplayStatics.h"
#include "./playerPawnBase.h"

void APlayerCtrlBase::BeginPlay()
{
	playerPawn = Cast<AplayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

void APlayerCtrlBase::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	/** Axis Mappings */

	InputComponent->BindAxis("MoveRight", this, &APlayerCtrlBase::MoveRight);

	/** Action Mappings */

	InputComponent->BindAction("Punch", IE_Pressed, this, &APlayerCtrlBase::Punch);
	InputComponent->BindAction("Kick", IE_Pressed, this, &APlayerCtrlBase::Kick);
	InputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCtrlBase::Jump);

	InputComponent->BindAction("Crouch", IE_Pressed, this, &APlayerCtrlBase::Crouch);
	InputComponent->BindAction("Crouch", IE_Released, this, &APlayerCtrlBase::Uncrouch);

	InputComponent->BindAction("Pause", IE_Pressed, this, &APlayerCtrlBase::Pause);
}

void APlayerCtrlBase::MoveRight(float value)
{
	playerPawn->MoveRight(value);
}

void APlayerCtrlBase::Punch()
{
	playerPawn->Attack(true, false);
}

void APlayerCtrlBase::Kick()
{
	playerPawn->Attack(false, false);
}

void APlayerCtrlBase::Jump()
{
	playerPawn->DoJump();
}

void APlayerCtrlBase::Crouch()
{
	playerPawn->DoCrouch();
}

void APlayerCtrlBase::Uncrouch()
{
	playerPawn->StopCrouch();
}

void APlayerCtrlBase::Pause()
{
	//TODO
}
