// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerCtrlBase.generated.h"

/**
 * 
 */
UCLASS()
class ETERNALECHOES_API APlayerCtrlBase : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite);
	class AplayerPawnBase* playerPawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void SetupInputComponent();

	void MoveRight(float value);

	void Punch();

	void Kick();

	void Jump();

	void Crouch();

	void Uncrouch();

	void Pause();

};

