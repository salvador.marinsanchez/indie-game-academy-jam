// Copyright Epic Games, Inc. All Rights Reserved.


#include "DarkLight_SistersGameModeBase.h"
#include "./playerPawnBase.h"
#include "Kismet/GameplayStatics.h"

ADarkLight_SistersGameModeBase::ADarkLight_SistersGameModeBase()
{
	//PrimaryActorTick.bStartWithTickEnabled = true;
	//PrimaryActorTick.bCanEverTick = true;
}

void ADarkLight_SistersGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AplayerPawnBase::StaticClass(), FoundActors);

	for (AActor* ac : FoundActors) {
		players.Add(Cast<AplayerPawnBase>(ac));
		
	}

	isReady = true;
	gameModeReady.Broadcast();
}

void ADarkLight_SistersGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADarkLight_SistersGameModeBase::GameFinished(bool isPlayer1Dead)
{
	gameFinishedEvent.Broadcast(isPlayer1Dead ? 2 : 1);
}

void ADarkLight_SistersGameModeBase::TimerRunOut()
{
	gameFinishedEvent.Broadcast(0);
}
