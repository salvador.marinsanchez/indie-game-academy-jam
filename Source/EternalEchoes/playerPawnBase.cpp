// Fill out your copyright notice in the Description page of Project Settings.


#include "playerPawnBase.h"
#include "DarkLight_SistersGameModeBase.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/GameplayStatics.h"

static const FName extraCollidersTag = FName("exCollider");;
static const FName baseBodyTag = FName("PlayerBody");

AplayerPawnBase::AplayerPawnBase()
{
 	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->ComponentTags.Add(baseBodyTag);

	brazoDBox = CreateDefaultSubobject<UBoxComponent>(TEXT("brazoDBox"));
	brazoDBox->SetupAttachment(GetMesh(), FName("RightHand"));
	brazoDBox->ComponentTags.Add(extraCollidersTag);

	brazoIBox = CreateDefaultSubobject<UBoxComponent>(TEXT("brazoIBox"));
	brazoIBox->SetupAttachment(GetMesh(), FName("LeftHand"));
	brazoIBox->ComponentTags.Add(extraCollidersTag);

	piernaDBox = CreateDefaultSubobject<UBoxComponent>(TEXT("piernaDBox"));
	piernaDBox->SetupAttachment(GetMesh(), FName("RightLeg"));
	piernaDBox->ComponentTags.Add(extraCollidersTag);

	piernaIBox = CreateDefaultSubobject<UBoxComponent>(TEXT("piernaIBox"));
	piernaIBox->SetupAttachment(GetMesh(), FName("LeftLeg"));
	piernaIBox->ComponentTags.Add(extraCollidersTag);

	air_short_box = CreateDefaultSubobject<UBoxComponent>(TEXT("air_short"));
	air_short_box->SetupAttachment(RootComponent);
	air_large_box = CreateDefaultSubobject<UBoxComponent>(TEXT("air_large"));
	air_large_box->SetupAttachment(RootComponent);
	mid_short_box = CreateDefaultSubobject<UBoxComponent>(TEXT("mid_short"));
	mid_short_box->SetupAttachment(RootComponent);
	mid_large_box = CreateDefaultSubobject<UBoxComponent>(TEXT("mid_large"));
	mid_large_box->SetupAttachment(RootComponent);
	low_short_box = CreateDefaultSubobject<UBoxComponent>(TEXT("low_short"));
	low_short_box->SetupAttachment(RootComponent);
	low_large_box = CreateDefaultSubobject<UBoxComponent>(TEXT("low_large"));
	low_large_box->SetupAttachment(RootComponent);

}

void AplayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	currentHp = maxHp;

	gm = Cast<ADarkLight_SistersGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	gm->gameModeReady.AddDynamic(this, &AplayerPawnBase::initOtherPlayer);
}

void AplayerPawnBase::initOtherPlayer()
{
	check(gm->players.Num() == 2)

	if (gm->players[0] == this) {
		otherPlayer = gm->players[1];
	}
	else {
		otherPlayer = gm->players[0];
	}

	CheckDistances();
}

void AplayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isDisabled) return;
	if (movMode == VerticalState::AIR) return;
	if (!canAttack) return;

	CheckDistances();
}

void AplayerPawnBase::AirAttack(bool isPunch, bool strength)
{
	PlayAnimMontage(isPunch ? air_short : air_large);
}

void AplayerPawnBase::MidAttack(bool isPunch, bool strength)
{
	PlayAnimMontage(isPunch ? mid_short : mid_large);
}

void AplayerPawnBase::LowAttack(bool isPunch, bool strength)
{
	PlayAnimMontage(isPunch ? low_short : low_large);
}

void AplayerPawnBase::DoCrouch()
{
	if (isDisabled) return;
	if (movMode == VerticalState::AIR) return;

	if (movMode == VerticalState::MID) {
		ChangeState(VerticalState::LOW);

		Crouch();
	}
}

void AplayerPawnBase::StopCrouch()
{
	if (isDisabled) return;
	if (movMode != VerticalState::LOW) return;

	ChangeState(VerticalState::MID);

	UnCrouch();
}

void AplayerPawnBase::MoveRight(float value)
{
	isBlockingAttack = false;

	if (isDisabled) return;
	if (movMode == VerticalState::AIR || !canAttack) return;

	if (value > 0 && !isLookingRight
		|| value < 0 && isLookingRight) {
		isBlockingAttack = true;

		value *= blockingMovementMultiplier;
	}

	AddMovementInput(moveRightDirection, value);
}

void AplayerPawnBase::DoJump()
{
	if (isDisabled) return;
	if (movMode == VerticalState::AIR) return;

	if (GetMovementComponent()->Velocity.Y != 0) {
		bool isPositivei = GetMovementComponent()->Velocity.Y > 0;

		GetMovementComponent()->Velocity.Y = GetMovementComponent()->GetMaxSpeed() * (isPositivei ? 1 : -1);
	}

	ChangeState(VerticalState::AIR);
	Jump();

}

void AplayerPawnBase::Attack(bool isPunch, bool strength)
{
	if (isDisabled) return;
	if (!canAttack) return;

	canAttack = false;
	currentAttackDamage = isPunch ? punchDamage : kickDamage;

	switch (movMode) {
	case VerticalState::AIR:
		AirAttack(isPunch, strength);
		break;

	case VerticalState::MID:
		MidAttack(isPunch, strength);
		break;

	case VerticalState::LOW:
		LowAttack(isPunch, strength);
		break;
	}
}

void AplayerPawnBase::AttackDamageWindowFinished()
{
	canDoDamage = false;
	DisableExColCollisions();
}
//0 air, 1 mid, 2 low
void AplayerPawnBase::AttackDamageShortWindowStart()
{
	canDoDamage = true;

	switch (currentAttackMode) {
	case VerticalState::AIR:
		CheckEnemiesInside(air_short_box);
		break;
	case VerticalState::MID:
		CheckEnemiesInside(mid_short_box);
		break;
	case VerticalState::LOW:
		CheckEnemiesInside(low_short_box);
		break;
	}
}
//0 air, 1 mid, 2 low
void AplayerPawnBase::AttackDamageLargeWindowStart()
{
	canDoDamage = true;

	switch (currentAttackMode) {
	case VerticalState::AIR:
		CheckEnemiesInside(air_large_box);
		break;
	case VerticalState::MID:
		CheckEnemiesInside(mid_large_box);
		break;
	case VerticalState::LOW:
		CheckEnemiesInside(low_large_box);
		break;
	}

}


void AplayerPawnBase::AttackAnimationFinished()
{
	canAttack = true;
	AttackDamageWindowFinished();
}

//movMode
void AplayerPawnBase::TryHit(float damage, VerticalState type, FHitResult hit)
{
	bool isRealHit = true;

	if (movMode == VerticalState::LOW && type != VerticalState::AIR) {
		isRealHit = false;
	}

	if (isBlockingAttack && movMode == VerticalState::MID && type != VerticalState::LOW) {
		isRealHit;
	}
	
	if (!isRealHit) {
		PlayAnimMontage(movMode == VerticalState::LOW ? low_block : mid_block);
		soundEvent.Broadcast(0);
	}
	else {
		//DO HIT
		GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Blue, FString::Printf(TEXT("Hit!")));
		currentHp -= damage;

		if (currentHp < 0) {
			currentHp = 0;
			gm->GameFinished(isPlayerOne);
		}

		HealthChangedEvent.Broadcast(currentHp);

		//STOP CURRENT ATTACK/ANIMATION
		StopAnimMontage();
		AttackDamageWindowFinished();
		canAttack = false;
		//PLAY HIT ANIMATION
		PlayAnimMontage(movMode == VerticalState::LOW ? low_hit : movMode == VerticalState::MID ? mid_hit : air_hit);
		ChangeState(VerticalState::MID);

	}
}

void AplayerPawnBase::OnColliderOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isEnemy(OtherComp)) {
		hitEnemy(OtherComp, SweepResult);
	}
}

void AplayerPawnBase::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);

	CheckDistances();
	ChangeState(VerticalState::MID);
}

void AplayerPawnBase::ChangeState(VerticalState newState)
{
	movMode = newState;
}

void AplayerPawnBase::CheckDistances()
{
	if (otherPlayer == nullptr) initOtherPlayer();

	float baseScale = GetMesh()->GetRelativeScale3D().X;

	if (isLookingRight && GetActorLocation().Y > otherPlayer->GetActorLocation().Y) {

		isLookingRight = false;
		FVector newForward = GetActorForwardVector() * -1;
		SetActorRotation(newForward.Rotation(), ETeleportType::ResetPhysics);
		GetMesh()->SetWorldScale3D(FVector(baseScale, baseScale, baseScale));
	}
	else if(!isLookingRight && GetActorLocation().Y < otherPlayer->GetActorLocation().Y){
		isLookingRight = true;
		FVector newForward = GetActorForwardVector() * -1;
		SetActorRotation(newForward.Rotation(), ETeleportType::ResetPhysics);
		GetMesh()->SetWorldScale3D(FVector(baseScale, -baseScale, baseScale));
	}
}

void AplayerPawnBase::CheckEnemiesInside(UBoxComponent* colliderToCheck)
{
	colliderToCheck->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	TArray<UPrimitiveComponent*> comps;
	colliderToCheck->GetOverlappingComponents(comps);

	for (UPrimitiveComponent* comp : comps) {
		if (isEnemy(comp)) {
			hitEnemy(comp, FHitResult());
		}
	}
}

void AplayerPawnBase::hitEnemy(UPrimitiveComponent* comp, FHitResult hit)
{
	AplayerPawnBase* enemy = Cast<AplayerPawnBase>(comp->GetOwner());
	enemy->TryHit(currentAttackDamage, currentAttackMode, hit);

	AttackDamageWindowFinished();
}

bool AplayerPawnBase::isEnemy(UPrimitiveComponent* comp)
{
	return comp->GetOwner() != this
		&& canDoDamage
		&& (comp->ComponentHasTag(extraCollidersTag)
			|| comp->ComponentHasTag(baseBodyTag));

}
//DEPRECATED
void AplayerPawnBase::DisableExColCollisions()
{
	brazoDBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	brazoIBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	piernaDBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	piernaIBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

