// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "playerPawnBase.generated.h"

UENUM(BlueprintType)
enum VerticalState
{
	LOW,
	MID,
	AIR
};

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthChanged, float, newHealth);

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDoSOund, int, soundKey);

UCLASS()
class ETERNALECHOES_API AplayerPawnBase : public ACharacter
{
	GENERATED_BODY()

public:

	//Colliders
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* brazoDBox;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* brazoIBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* piernaDBox;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* piernaIBox;

	//Attack colliders
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* air_short_box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* air_large_box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* mid_short_box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* mid_large_box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* low_short_box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* low_large_box;

	//Animaciones
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* air_short;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* air_large;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* mid_short;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* mid_large;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* low_short;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* low_large;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* air_hit;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* mid_hit;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* low_hit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* low_block;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
	UAnimMontage* mid_block;

	UPROPERTY(BlueprintAssignable)
	FHealthChanged HealthChangedEvent;
	UPROPERTY(BlueprintAssignable)
	FDoSOund soundEvent;

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<VerticalState> movMode = VerticalState::MID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool canAttack = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector moveRightDirection = FVector(1, 0, 0);

	FTimerHandle debugAttackHandle;
	FTimerHandle debugJumpHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isDisabled = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float maxHp = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float blockingMovementMultiplier = 0.8f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float punchDamage = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float kickDamage = 7;

	UPROPERTY(BlueprintReadOnly)
	bool isBlockingAttack = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isPlayerOne = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool hasBeenHit = false;

	UPROPERTY(BlueprintReadOnly)
	bool isLookingRight = true;

	UPROPERTY(BlueprintReadOnly)
	AActor* otherPlayer = nullptr;

private:
	bool canDoDamage = false;

	TEnumAsByte<VerticalState> currentAttackMode = VerticalState::MID;
	float currentAttackDamage = 0;

	
	float currentHp = 0;

	class ADarkLight_SistersGameModeBase* gm;
public:
	// Sets default values for this character's properties
	AplayerPawnBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintCallable)
	void initOtherPlayer();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AirAttack(bool isPunch, bool strength = false);

	void MidAttack(bool isPunch, bool strength = false);

	void LowAttack(bool isPunch, bool strength = false);

	UFUNCTION(BlueprintCallable)
	void DoCrouch();

	UFUNCTION(BlueprintCallable)
	void StopCrouch();

	UFUNCTION(BlueprintCallable)
	void MoveRight(float value);

	UFUNCTION(BlueprintCallable)
	void DoJump();

	UFUNCTION(BlueprintCallable)
	void Attack(bool isPunch, bool strength);

	UFUNCTION(BlueprintCallable)
	void AttackDamageWindowFinished();

	UFUNCTION(BlueprintCallable)
	void AttackDamageShortWindowStart();
	UFUNCTION(BlueprintCallable)
	void AttackDamageLargeWindowStart();

	UFUNCTION(BlueprintCallable)
	void AttackAnimationFinished();

	UFUNCTION()
	void TryHit(float damage, VerticalState type, FHitResult hit);

	UFUNCTION()
	void OnColliderOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
protected:
	virtual void Landed(const FHitResult& Hit) override;

private:
	void ChangeState(VerticalState newState);

	void CheckDistances();

	void CheckEnemiesInside(class UBoxComponent* colliderToCheck);

	void hitEnemy(UPrimitiveComponent* comp, FHitResult hit);

	bool isEnemy(class UPrimitiveComponent* comp);

	void DisableExColCollisions();
};
