// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AIctrlBase.generated.h"

UENUM(BlueprintType)
enum AIStates
{
	hit,
	acercarse,
	huir,
	idle,
	none,
	agachado,
};

UCLASS()
class ETERNALECHOES_API AAIctrlBase : public AAIController
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AplayerPawnBase* pawnBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<AIStates> state = AIStates::none;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool hasBeenHit = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void pawnHit(float newHp);

	UFUNCTION(BlueprintCallable)
	void changeState(AIStates newState);
};
